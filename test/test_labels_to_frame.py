import pathlib
from ast import literal_eval
from src import labels_to_frames


directory = pathlib.Path(__file__).parent.parent


def test_fill_act_frame():
    result: dict = labels_to_frames.fill_act_frame(
        [('[CLS]', 'ACTOR'), ('De', 'ACTOR'), ('Afdeling', 'ACTOR'), ('bestuursrecht', 'ACTOR'), ('##spraak', 'ACTOR'),
         ('van', 'ACTOR'), ('de', 'ACTOR'), ('Raad', 'ACTOR'), ('van', 'ACTOR'), ('State', 'ACTOR'), ('behandelt', 'V'),
         ('het', 'OBJ'), ('hoger', 'OBJ'), ('beroep', 'OBJ'), ('aan', 'O'), ('de', 'REC'), ('officier', 'REC'),
         ('van', 'REC'), ('justitie', 'REC'), ('met', 'O'), ('toepassing', 'O'), ('van', 'O'),
         ('.', 'O'), ('afdeling', 'O'), ('8', 'O'), ('.', 'O'), ('2', 'O'), ('.', 'O'), ('3', 'O'), ('van', 'O'),
         ('de', 'O'), ('Algemene', 'O'), ('wet', 'O'), ('bestuursrecht', 'O'), ('[SEP]', 'O')])

    assert type(result) == dict
    assert result['action'] == 'behandelt'
    assert result['actor'] == 'de afdeling bestuursrechtspraak van de raad van state'
    assert result['recipient'] == 'de officier van justitie'
    assert result['act'] == 'behandelt het hoger beroep'
    assert result['object'] == 'het hoger beroep'


# todo: fix this test
# def test_create_frames():
#     result: dict = labels_to_frames.create_frames(directory / "src" / "resource" / "annotations" / "test.csv",
#                                                   directory / "src" / "resource" / "annotations" / "test_output.json")
#
#     assert len(result['acts']) > 10
#     assert type(result['acts']) == list
#     assert len(result['facts']) == 0


def test_merge_action_verbs():
    result_1: str = labels_to_frames.merge_action_verbs(['wordt', 'aange', '##haal', '##d'])
    result_2: str = labels_to_frames.merge_action_verbs(['is', 'nog', 'niet', 'in', 'werking', 'ge', '##treden'])
    result_3: str = labels_to_frames.merge_action_verbs(['plaatsvind', '##t'])
    result_4: str = labels_to_frames.merge_action_verbs(['zijn', 'gede', '##n', '##tif', '##ice', '##erd', 'geregistreerd'])
    result_5: str = labels_to_frames.merge_action_verbs(['wordt', 'ge', '##he', '##ven'])

    assert result_1 == 'wordt aangehaald'
    assert result_2 == 'is nog niet in werking getreden'
    assert result_3 == 'plaatsvindt'
    assert result_4 == 'zijn gedentificeerd geregistreerd'
    assert result_5 == 'wordt geheven'


def test_merge_actor_words():
    result_1: str = labels_to_frames.merge_actor_words(['de', 'vreemdelingen', '##administratie'])
    result_2: str = labels_to_frames.merge_actor_words(['over', '##treding', 'van', 'een', 'voor', '##schrift'])
    result_3: str = labels_to_frames.merge_actor_words(['bijzonder', '##e', '##e', 'aard'])
    result_4: str = labels_to_frames.merge_actor_words(['de', 'betrokken', '##e'])

    assert result_1 == 'de vreemdelingen administratie'
    assert result_2 == 'over treding van een voor schrift'
    assert result_3 == 'bijzonder e e aard'
    assert result_4 == 'de betrokken e'


def test_remove_bert_separator():
    test_sentence = literal_eval("[('[CLS]', 'O'), ('buiten', 'ACTOR'), ('##grenzen', 'ACTOR'), (':', 'O')"
                                 ", ('de', 'ACTOR'), ('Nederlandse', 'ACTOR'), ('ze', 'ACTOR'), ('##e', 'ACTOR')"
                                 ", ('##grenzen', 'ACTOR'), (',', 'O'), ('als', 'O'), ('##mede', 'O')"
                                 ", ('lucht', 'ACTOR'), ('\', 'O'), ('of', 'O'), ('ze', 'ACTOR'), ('##e', 'ACTOR')"
                                 ", ('##haven', 'O'), ('##s', 'O'), ('waar', 'O'), ('grens', 'OBJ')"
                                 ", ('##controle', 'OBJ'), ('op', 'O'), ('personen', 'O'), ('wordt', 'V')"
                                 ", ('uitgeoefend', 'V'), ('[SEP]', 'O')]")

    result_1: list = labels_to_frames.remove_bert_separator(test_sentence)
    # list of all words inside test_sentence
    result_2 = [x[0] for x in result_1]
    assert "buitengrenzen" in result_2
    assert "zeegrenzen" in result_2
    assert "alsmede" in result_2
    assert "zeehavens" in result_2
    assert "grenscontrole" in result_2
    # the sentence containing BERT tokens should be bigger than the sentence w/o BERT tokens
    assert len(test_sentence) > len(result_1)
