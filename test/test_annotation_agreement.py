import pandas as pd
from src.evaluation.calculate_agreement import calculate_fleiss_kappa


def test_kappa_agreement():
    test_annotations = pd.read_csv('..\\test\\test_data\\test_fleiss_kappa.csv')
    test_annotations = test_annotations.to_numpy()
    kappa = calculate_fleiss_kappa(test_annotations)

    assert kappa == 0.204


def preprocess_annotations_test(df):
    # add column specifying the sentence id
    df['sentence_id'] = (['wet_'+str(i) for i in range(0, len(df))])

    # there was a mistake in the test_csv: length of the token list is not always equal to length of srl tags list
    for index, row in df.iterrows():
        # if list of tokens longer than list of srl tags, then shorten the list of tokens to the length of srl tags list
        if len(row['tokens']) > len(row['srl_tags']):
            del row['tokens'][len(row['srl_tags']):]

        # if list of srl tags longer than list of tokens, then shorten the list of srl tags to the length of tokens list
        elif len(row['srl_tags']) > len(row['tokens']):
            del row['srl_tags'][len(row['tokens']):]

    # repeat df (as if sentences are annotated twice)
    df_repeated = pd.concat([df] * 2, ignore_index=True)

    return df_repeated


# todo: fix this test
# def test_annotation_agreement_preprocessing():
#     """
#     If the preprocessing is done correctly, the kappa will be exactly one
#     (because the two annotations for each sentence_id are exactly the same)
#     """
#     test_annotations = pd.read_csv('test_data/test_annotation_tool_output.csv',
#                                    converters={'tokens': eval, 'srl_tags': eval})
#
#     test_annotations = preprocess_annotations_test(test_annotations)
#     test_annotations = preprocess_annotations(test_annotations)
#     kappa = calculate_fleiss_kappa(test_annotations)
#
#     assert kappa == 1
