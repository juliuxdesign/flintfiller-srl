import pathlib
import tempfile
from src import label_text

directory = pathlib.Path(__file__).parent


# def update_expected_regression():
#     expected_path = "test_data/expected_regression_vreemdelingenwet_srl_2021.csv"
#
#     label_text.label_sentences(directory / "test_data" / "vreemdelingenwet_preprocessed_srl.csv",
#                                "GroNLP/bert-base-dutch-cased",
#                                expected_path)


def test_label_text_2021_regression():
    with tempfile.NamedTemporaryFile() as result:
        expected_path = directory / "test_data" / "expected_regression_vreemdelingenwet_srl_2021.csv"
        result_path = result.name + ".csv"
        label_text.label_sentences(directory / "test_data" / "vreemdelingenwet_preprocessed_srl.csv",
                                   "GroNLP/bert-base-dutch-cased",
                                   "C:/Users/bakkerrm/git/flintfiller-srl/src/resource/models/bertje_2021_e4",
                                   result_path)

        assert [row for row in open(result_path)] == [row for row in open(expected_path)]


def test_label_text_2022_regression():
    with tempfile.NamedTemporaryFile() as result:
        expected_path = directory / "test_data" / "expected_regression_vreemdelingenwet_srl_2022.csv"
        result_path = result.name + ".csv"
        label_text.label_sentences(directory / "test_data" / "vreemdelingenwet_preprocessed_srl.csv",
                                   "GroNLP/bert-base-dutch-cased",
                                   "C:/Users/bakkerrm/git/flintfiller-srl/src/resource/models/bertje_2022_e4",
                                   result_path)

        assert [row for row in open(result_path)] == [row for row in open(expected_path)]
