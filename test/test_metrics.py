from src.evaluation.metrics import create_df_all_predictions, calculate_accuracies


def test_metrics_bertje2021():
    df, df_rlb, df_rlb_srl, df_rlb_old = create_df_all_predictions(
        "test_data/predicted_labels_bertje2021_groundtruth.csv",
        "test_data/predicted_labels_groundtruth_rulebased.csv",
        "test_data/predicted_labels_hybrid_srl_rlb_option_one.csv",
        "test_data/predicted_labels_groundtruth_rulebased_old.csv",
        "test_data/ground_truth.csv")
    result = calculate_accuracies(df, df_rlb, df_rlb_srl, df_rlb_old)

    assert result == [0.7194507994937207, 0.5868025336810704, 0.8424932905673023, 0.45357889111436056]


def test_metrics_bertje2022():
    df, df_rlb, df_rlb_srl, df_rlb_old = create_df_all_predictions(
        "test_data/predicted_labels_bertje2022_groundtruth.csv",
        "test_data/predicted_labels_groundtruth_rulebased.csv",
        "test_data/predicted_labels_hybrid_srl_rlb_option_one.csv",
        "test_data/predicted_labels_groundtruth_rulebased_old.csv",
        "test_data/ground_truth.csv")
    result = calculate_accuracies(df, df_rlb, df_rlb_srl, df_rlb_old)

    assert result == [0.8425439974695428, 0.5868025336810704, 0.8424932905673023, 0.45357889111436056]
