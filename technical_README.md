# FlintFiller-srl scripts

This project is created to automatically create Flint Frames using a semantic role labelling technique. 
This is the technical README.


## pyproject.toml
This is the file that states the packages that have to be downloaded to make the code run. 
Below, it is detailed how to develop and manage dependencies with poetry. Installing the requirements is explained in the general README.md

### Development 
#### dependency management
- `poetry update` # Update to latest version of dependencies as specified in [pyproject.toml](pyproject.toml)
- `poetry add [packagename] --dev` # Install and document new package (--dev optional if development package)
- `poetry remove [packagename]` # Remove package
- `poetry run [command]` # Enters venv and then executes specified command
- `poetry shell` #activate virtual environment

## License

FlintFiller is released under Apache 2.0, for more information see the LICENSE.
