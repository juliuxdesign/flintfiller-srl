# FlintFiller

This project is created to automatically create Flint Frames. You can choose between two options of creating the flint
frames, (1) a rule-based approach which uses syntactical tags and rules to define the elements for the frames, and (2) a
machine learning based approach where a model learns to add semantic role labels to words in a sentence
(Semantic Role Labeling). This model is then applied on your textual data of choice. This repo contains the code for
the srl-based approach. If you want to use the rule-based method, please go to
[the FlintFiller-RLB project](https://gitlab.com/normativesystems/flintfillers/flintfiller-rlb).

This repository contains:
- [Finetuned BERTje models](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl/-/tree/master/src/resource/models) (finetuned on annotated law texts)
- [Code for finetuning a BERTje model](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl/-/tree/master/src/finetune_bertje)
- [Code for evaluating the model](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl/-/tree/master/src/evaluation) (still work-in-progress)
- [A script to add semantic role labels to your text of choice](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl/-/blob/master/src/label_text.py)
- Annotated data that was used to finetune the BERTje models in [dataset object](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl/-/tree/master/src/resource/finetuning_data) or [csv](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl/-/tree/master/src/resource/annotations)

## Table of Contents
- [FlintFiller](#flintfiller)
  - [Table of Contents](#table-of-contents)
  - [Usage](#usage)
    - [Getting started](#getting-started)
  - [detailed in 'getting started', including the step in brackets.](#detailed-in-getting-started-including-the-step-in-brackets)
    - [Finetuning your own model](#finetuning-your-own-model)
    - [Using the pretrained and finetuned BERTje model on text](#using-the-pretrained-and-finetuned-bertje-model-on-text)
      - [Adding semantic role labels and creating flint frames](#adding-semantic-role-labels-and-creating-flint-frames)
      - [Adding semantic role labels only](#adding-semantic-role-labels-only)
      - [Creating flint frames only](#creating-flint-frames-only)
  - [Documentation](#documentation)
  - [Maintainers](#maintainers)
  - [License](#license)

## Usage

### Getting started

- install and activate python 3.9.0*
- install poetry if new to poetry: https://python-poetry.org/docs/#installation
- (`poetry env use /full/path/to/python`)
- `poetry config virtualenvs.in-project true`
- `poetry install`
- Go to project settings of your editor and set the created virtual environment as project interpreter
- **Important:** this tool uses python 3.8 or higher because the transformers and the matplotlib package only support python 3.8 or higher. Running
  FlintFiller-srl with a lower python version results in an error.

---
\* type `python --version` to check which version of python you are using if you're not sure. If your version is not
python 3.8, you will have to change it. FlintFiller-rlb uses python 3.6 and FlintFiller-srl uses python 3.9, so you will
need different versions of Python. You can either (1) switch between Python versions and then create your environment
with Poetry, or (2) tell Poetry which version to use for the current project environment:

1. To switch between Python versions, we recommend the use of
[pyenv](https://github.com/pyenv-win/pyenv-win) to make your life easier. When installed and working, change the python
version using pyenv to 3.6 There are other approaches such as
[this one](https://www.freecodecamp.org/news/installing-multiple-python-versions-on-windows-using-virtualenv/),
[this thread](https://stackoverflow.com/questions/52584907/how-to-downgrade-python-from-3-7-to-3-6/65533322), or
[this thread](https://stackoverflow.com/questions/5087831/how-should-i-set-default-python-version-in-windows), but you
will need to execute more manual steps. After activating the correct version of Python, follow the steps detailed in '
getting started'.    
2. To control the version of Python when creating an environment with Poetry, simply pass another line of code in your
command line, as documented [here](https://python-poetry.org/docs/managing-environments/). To do this, follow the steps
detailed in 'getting started', including the step in brackets.
---

### Finetuning your own model

To finetune your own model, you have to gather labeled data. Assuming you have a suitable dataset, you have to run the
preprocess script and the finetune script in src/finetune_bertje. The preprocess script takes as arguments your file
with annotated data, and an output path. The result is a preprocessed dataset required for the finetuning script. To
call the preprocess script you can run the following command in your terminal:

```bash
python src/finetune_bertje/preprocess_data.py \
-d src/resource/annotations/annotation_2021/all_positives_with_tags_full.csv \
-o src/resource/finetuning_data/test_dataset
```

Now you have prepared your data for finetuning, and you can run the finetuning script. Finetune_BERTje.py takes three
arguments, the pretrained model on which you want to add a finetuned layer, the dataset which you want to use for
finetuning, and an output folder where you want your finetuned model to be stored. To call the finetune script you can
run the following command in your terminal:

```bash 
python src/finetune_bertje/finetune_BERTje.py \
-m GroNLP/bert-base-dutch-cased \
-d src/resource/finetuning_data/dataset_2021 \
-o src/resource/models/test_model
```

### Using the pretrained and finetuned BERTje model on text

#### Adding semantic role labels and creating flint frames
If you want to add semantic role labels to your text using our pretrained and finetuned BERTje model (stored in
src/resource/models/bertje_e4) and create flint frames, you can use the main.py script with the following (example)
command in your terminal:

```bash
python src/main.py \
-d src/resource/to_be_labeled/ground_truth.csv \
-t GroNLP/bert-base-dutch-cased \
-m src/resource/models/bertje_2022_e4 \
-o src/resource/labeled_results/predicted_labels_bert2022_groundtruth.csv \
-f src/resource/labeled_results/flint_output_bert2022_groundtruth.csv
```

with arguments (choose between -s or -d):

- `-d`/`--data` the input data file containing the sentences that you want to label
- `-s`/`--sequence` an input sentence (you can use this argument for faster testing)
- `-t`/`--tokenizer` the base tokenizer you want to use (default = 'GroNLP/bert-base-dutch-cased')
- `-m`/`--model` your finetuned model
- `-o`/`--output` the desired output file for the .csv with predicted labels
- `-f`/`--flintoutput` the desired output file for the flint frames

The data argument accepts 
1) csv files containing a 'Brontekst' column, which can be created from law texts using the juridecomposer 
(part of flintfiller-rlb), e.g. `src/resource/to_be_labeled/Vreemdelingenwet_srl.csv`
2) a dataset object, e.g. `src/resource/finetuning_data/dataset_2022`

The sequence argument accepts a string and is useful for fast testing.

#### Adding semantic role labels only
Alternatively, if you just want to add semantic role labels to your text using our pretrained and finetuned BERTje
model (stored in src/resource/models/bertje_e4), you can use the label_text.py script with the following (example)
command in your terminal:

```bash
python src/label_text.py \
-d src/resource/to_be_labeled/Vreemdelingenwet_srl.csv \
-t GroNLP/bert-base-dutch-cased \
-m src/resource/models/bertje_2022_e4 \
-o src/resource/labeled_results/predicted_labels_vreemdelingenwet.csv 
```

with arguments (choose between -s or -d):

- `-d`/`--data` the input data file containing the sentences that you want to label
- `-s`/`--sequence` an input sentence (you can use this argument for faster testing)
- `-t`/`--tokenizer` the base tokenizer you want to use (default = 'GroNLP/bert-base-dutch-cased')
- `-o`/`--output` the desired output file for the .csv with predicted labels

The data argument accepts 
1) csv files containing a 'Brontekst' column, which can be created from law texts using the juridecomposer 
(part of flintfiller-rlb), e.g. `src/resource/to_be_labeled/Vreemdelingenwet_srl.csv`
2) a dataset object, e.g. `src/resource/finetuning_data/dataset_2022`

The sequence argument accepts a string and is useful for fast testing.

#### Creating flint frames only
Finally, if you want to transform a .csv file with predicted labels into flint frames, the labels_to_frames.py script
can also be used as a standalone script:

```bash
python src/labels_to_frames.py \
-d src/resource/labeled_results/predicted_labels_vreemdelingenwet11-4.csv \
-o src/resource/labeled_results/flint_frames_vreemdelingenwet11-4.json
```

with arguments:

- `-d`/`--data` data path, needs to be a csv file containing the outcome of label_text.py
- `-o`/`--output` the desired output file for the flint frames

For finetuning the model and more explanation on the other scripts please read the technical README.

## Documentation

- [R.M. Bakker, R.A.N. van Drie, M.H.T. de Boer, R. van Doesburg, and T.M. van Engers. “Semantic Role Labelling for Dutch Law Texts”. In: Proceedings of the Language Resources and Evaluation Conference. Marseille, France](https://aclanthology.org/2022.lrec-1.47/).

## Maintainers

Roos Bakker, TNO (roos.bakker@tno.nl)

Romy van Drie, TNO (romy.vandrie@tno.nl)

Daan Vos, TNO (daan.vos@tno.nl)

Maaike de Boer, TNO (maaike.deboer@tno.nl)

## License

FlintFiller is released under Apache 2.0, for more information see the LICENSE.

