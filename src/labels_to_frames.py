"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Maaike de Boer, Roos Bakker, Romy van Drie
    @contact: maaike.deboer@tno.nl, roos.bakker@tno.nl, romy.vandrie@tno.nl
"""
import argparse
import json
import pathlib
import re
from ast import literal_eval

import pandas as pd

directory = pathlib.Path(__file__).parent


def read_csv_to_df(csv_file):
    df = pd.read_csv(csv_file)
    print("csv loaded from ", csv_file)
    return df


def write_df_to_csv(df, fle):
    df.to_csv(fle, sep=';')
    print("df written to " + fle)


def create_empty_flint_format() -> dict:
    flint_format = {
        "acts": [],
        "facts": [],
        "duties": []
    }
    return flint_format


def create_empty_act_frame() -> dict:
    act_frame = {
        "act": "",
        "actor": "",
        "action": "",
        "object": "",
        "recipient": "",
        "preconditions": {
            "expression": "LITERAL",
            "operand": True
        },
        "create": [],
        "terminate": [],
        "sources": [],  # with validFrom, validTo, citation juriconnect and text
        "explanation": ""
    }
    return act_frame


def create_empty_fact_frame() -> dict:
    fact_frame = {
        "fact": "",
        "function": [],
        "sources": [],  # with validFrom, validTo, citation juriconnect and text
        "explanation": ""
    }
    return fact_frame


def write_flint_frames_to_json(flint_frames, flint_file):
    with open(str(flint_file), 'w') as f:
        json.dump(flint_frames, f)
    print("flint frames written to " + str(flint_file))


def merge_action_verbs(split_verbs: list) -> str:
    merged_verb = ' '.join(split_verbs)
    if ' ##' in merged_verb:
        merged_verb = merged_verb.replace(' ##', '')
    elif '##' in merged_verb:
        merged_verb = merged_verb.replace('##', '')
    return merged_verb


def merge_actor_words(split_words: list) -> str:
    merged_words = []
    for word in split_words:
        if ' ##' in word:
            merged_words.append(word.replace(' ##', ''))
        elif '##' in word:
            merged_words.append(word.replace('##', ''))
        else:
            merged_words.append(word)
            continue
    merged_words = ' '.join(merged_words)
    return merged_words


def remove_bert_separator(labeled_sentence):
    reconstructed_sentence = []
    current_position = 0
    for token, label in labeled_sentence:
        # preprocess to remove specific characters
        token = re.sub(r"[$()/\\]", "", token)
        # in case a token such as "(" appears, the token gets stripped to an empty string.
        # continue the loop in that case
        if not token:
            continue
            
        # current word is not a bert token
        if token.startswith("##") == False:
            # this is the last non-bert token, which means that it is the start of a bert token
            non_bert_token_pos = current_position
            # make a list instead of tuple due tuple inmutability
            reconstructed_sentence.append([token, label])
            current_position += 1
        # current word is a split bert token (starting with ##)
        else:
            reconstructed_sentence[non_bert_token_pos][0] = reconstructed_sentence[non_bert_token_pos][0] \
                                                            + token.replace('##', '')
    return reconstructed_sentence


def fill_act_frame(labeled_sentence: list) -> dict:
    act_frame = create_empty_act_frame()
    actor_words = ""
    object_words = ""
    action_words = ""
    recipient_words = ""

    # labeled sentence unberten
    labeled_sentence = remove_bert_separator(labeled_sentence)

    for labeled_word in labeled_sentence[1:]:
        if labeled_word[1] == "ACTOR":
            actor_words = actor_words + " " + (labeled_word[0].lower())
        elif labeled_word[1] == "OBJ":
            object_words = object_words + " " + (labeled_word[0].lower())
        elif labeled_word[1] == "V":
            action_words = action_words + " " + (labeled_word[0].lower())
        elif labeled_word[1] == "REC":
            recipient_words = recipient_words + " " + (labeled_word[0].lower())
        else:
            continue

    # TODO: Fix: not all action words should be merged since sentences sometimes contain multiple acts or multiple
    #            separate verb clauses.
    act_frame.update(
        {'actor': actor_words.lstrip(),
         'object': object_words.lstrip(),
         'action': action_words.lstrip(),
         'recipient': recipient_words.lstrip(),
         'act': action_words.lstrip() + object_words})
    return act_frame


def create_frames(labeled_csv, output_location):
    flint_format = create_empty_flint_format()
    df = read_csv_to_df(labeled_csv)
    for labeled_sentence in df.iloc[:, 2]:
        act_frame = fill_act_frame(literal_eval(labeled_sentence))
        # only add act frame if the action is not empty
        if bool(act_frame["action"]):
            flint_format['acts'].append(act_frame)
        else:
            print("empty act frame skipped: {}".format(act_frame))
    write_flint_frames_to_json(flint_format, output_location)
    return flint_format


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--annotations',
                        help="annotations path, needs to be a csv file containing the outcome of label_text.py")
    parser.add_argument('-o', '--output',
                        help="desired save location of your flint frames")
    arguments = parser.parse_args()
    return arguments


if __name__ == '__main__':
    args = parse_commandline_arguments()
    if args.data and args.output:
        create_frames(args.data, args.output)
    else:
        print('please enter the path of your labeled annotations and a desired output location.')
