import argparse
import csv
import string
import pathlib

import torch
from datasets import load_from_disk
from transformers import AutoTokenizer, AutoModelForTokenClassification
import pandas as pd

directory = pathlib.Path(__file__).parent


def label_sequence(sequence, tokenizer, model):
    """
    Can be used to label a single text sequence, as opposed to multiple sequences (in a csv file or a dataset object).
    - If you want to label text sequences in a csv file, use label_sentences().
    - If you want to label text sequences in a dataset object, use label_encoded_set().

    Args:
        sequence: str
        tokenizer: the tokenizer used to tokenized your text input. Please use the default.
        model: the path to your finetuned BERTje model
    """
    model = AutoModelForTokenClassification.from_pretrained(model)
    tokenizer = AutoTokenizer.from_pretrained(tokenizer)
    label_list = ["O", "V", "ACTOR", "OBJ", "REC"]

    tokens = tokenizer.tokenize(tokenizer.decode(tokenizer.encode(sequence)))
    inputs = tokenizer.encode(sequence, return_tensors="pt")

    outputs = model(inputs)[0]
    predictions = torch.argmax(outputs, dim=2)

    print([(token, label_list[prediction]) for token, prediction in zip(tokens, predictions[0].tolist())])


def label_encoded_set(data_path, tokenizer, model, output_path):
    """
    Can be used to label text sequences in a dataset object (can be created in finetuning_data > preprocess_data.py)
    - If you want to label text sequences in a csv file, use label_sentences().
    - If you want to label a single text sequence (str), use label_sequence().

    Args:
        data_path: the path to the data file to be labeled
        tokenizer: the tokenizer used to tokenized your text input. Please use the default.
        model: the path to your finetuned BERTje model
        output_path: the path for the output file
    """
    encoded_dataset = load_from_disk(data_path)
    model = AutoModelForTokenClassification.from_pretrained(model)
    tokenizer = AutoTokenizer.from_pretrained(tokenizer)
    label_list = ["O", "V", "ACTOR", "OBJ", "REC"]

    with open(output_path, 'w', encoding='UTF8', newline='') as f:
        # create the csv writer
        writer = csv.writer(f)

        for sequence in encoded_dataset['test']['tokens']:
            sequence = ' '.join(sequence)
            sequence = sequence.translate(str.maketrans('', '', string.punctuation))
            tokens = tokenizer.tokenize(tokenizer.decode(tokenizer.encode(sequence)))
            inputs = tokenizer.encode(sequence, return_tensors="pt")

            outputs = model(inputs)[0]
            predictions = torch.argmax(outputs, dim=2)

            writer.writerow([sequence, [(token, label_list[prediction]) for token, prediction in
                                        zip(tokens, predictions[0].tolist())]])


def label_sentences(data_path, tokenizer, model, output_path, include_probabilities: bool = False):
    """
    Can be used to label text sequences in a csv file.
    - If you want to label text sequences in dataset object, use label_encoded_set().
    - If you want to label a single text sequence (str), use label_sequence().

    Args:
        data_path: the path to the data file to be labeled.
        tokenizer: the tokenizer used to tokenized your text input. Please use the default.
        model: the (finetuned) model that you want to use.
        output_path: the path for the output file.
        include_probabilities: boolean for including probabilities.
    """
    df = pd.read_csv(data_path, sep=";")
    model = AutoModelForTokenClassification.from_pretrained(model)
    tokenizer = AutoTokenizer.from_pretrained(tokenizer)
    label_list = ["O", "V", "ACTOR", "OBJ", "REC"]

    if {'sentence_id'}.issubset(df.columns):
        label_ground_truth(df, include_probabilities, label_list, model, output_path, tokenizer)
    elif {'Nummer'}.issubset(df.columns):
        label_juridecomposed_law(df, label_list, model, output_path, tokenizer)
    else:
        print(data_path, 'is not in a known format to label it')


def label_juridecomposed_law(df, label_list, model, output_path, tokenizer):
    with open(output_path, 'w', encoding='UTF8', newline='') as f:
        # create the csv writer
        writer = csv.writer(f, delimiter=';')

        for sequence in df['Brontekst']:
            tokens = tokenizer.tokenize(tokenizer.decode(tokenizer.encode(sequence)))
            inputs = tokenizer.encode(sequence, return_tensors="pt")

            outputs = model(inputs)[0]
            predictions = torch.argmax(outputs, dim=2)

            writer.writerow([sequence, [(token, label_list[prediction]) for token, prediction in
                                        zip(tokens, predictions[0].tolist())]])


def label_ground_truth(df, include_probabilities, label_list, model, output_path, tokenizer):
    with open(output_path, 'w', encoding='UTF8', newline='') as f:
        # create the csv writer
        writer = csv.writer(f)

        writer.writerow(['sentence_id', 'Brontekst', 'predicted_labels'])

        for index, row in df.iterrows():
            tokens = tokenizer.tokenize(tokenizer.decode(tokenizer.encode(row['Brontekst'])))
            inputs = tokenizer.encode(row['Brontekst'], return_tensors="pt")

            # extract the logits https://developers.google.com/machine-learning/glossary/#logits
            outputs = model(inputs)[0]
            predictions = torch.argmax(outputs, dim=2)
            full_output = model(inputs)

            # potential idea to rescale logits:
            # http://www.kasimte.com/2020/02/14/how-does-temperature-affect-softmax-in-machine-learning.html
            if include_probabilities:
                writer.writerow([row['sentence_id'],
                                 row['Brontekst'],
                                 [(token, label_list[prediction], max(torch.softmax(probability, dim=0).tolist())) for
                                  token, prediction, probability in
                                  zip(tokens, predictions[0].tolist(), full_output.logits[0])]])
            else:
                writer.writerow([row['sentence_id'],
                                 row['Brontekst'],
                                 [(token, label_list[prediction]) for token, prediction in
                                  zip(tokens, predictions[0].tolist())]])


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data',
                        help="data path")
    parser.add_argument('-s', '--seq',
                        help="sequence")
    parser.add_argument('-t', '--tokenizer',
                        help="pretrained model as tokenizer")
    parser.add_argument('-m', '--model',
                        help="your finetuned model")
    parser.add_argument('-o', '--output',
                        help="save location")
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = parse_commandline_arguments()
    if args.data:
        if args.data.endswith('.csv'):
            label_sentences(args.data, args.tokenizer, args.model, args.output, False)
        else:
            label_encoded_set(args.data, args.tokenizer, args.model, args.output)
    elif args.seq:
        label_sequence(args.seq, args.tokenizer, args.model)
    else:
        print('please enter the path of your data or a sequence.')
