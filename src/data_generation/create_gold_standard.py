import pandas as pd
from data_generation.create_clean_dataset import remove_unwanted_annotations, remove_duplicate_identical_annotations, \
    remove_annotations_wrong_shape
from data_generation.create_clean_dataset import get_law


def remove_unwanted_laws(annotations):
    # drop laws
    law_column = annotations.apply(lambda row: get_law(row), axis=1)
    annotations = annotations.assign(law=law_column.values)
    laws_in_gold_standard = ['Wet_openbaarheid_van_bestuur', 'Rijkswet_op_het_Nederlanderschap']
    gold_standard = annotations[annotations['law'].isin(laws_in_gold_standard)]

    return gold_standard


def main():
    annotations = pd.read_csv('../resource/annotations/annotation_2022/all_annotations_merged.csv',
                              converters={'tokens': eval, 'srl_tags': eval},
                              sep=';', encoding='utf-8')

    ground_truth_control_sentences = pd.read_csv('../resource/annotations/annotation_2022/control_sentences.csv',
                                                 converters={'tokens': eval, 'srl_tags': eval},
                                                 encoding='ANSI', sep=";")

    annotations = remove_unwanted_annotations(annotations)
    annotations = remove_unwanted_laws(annotations)
    annotations = remove_duplicate_identical_annotations(annotations)
    annotations = remove_annotations_wrong_shape(annotations)
    ground_truth = pd.concat([ground_truth_control_sentences, annotations])

    # NOTE: this script generates output that needs to be checked manually for the annotation data
    # (control sentences were already checked earlier)
    ground_truth.to_csv('../resource/evaluation/ground_truth_to_be_checked.csv',
                        index=False, encoding='utf-8', sep=';')


if __name__ == '__main__':
    main()
