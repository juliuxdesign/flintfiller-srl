import pandas as pd


def remove_unwanted_annotations(annotations):
    # drop annotation round OLE, because 1) pilot and 2) did not have a high enough inter-annotator agreement
    annotations = annotations[annotations['annotation_round'] != 'OLE']

    return annotations


def get_law(row):
    return row['sentence_id'].split('-')[-1]


def remove_sentences_from_gold_standard(annotations):
    # drop test sentences from dataset
    annotations = annotations[~annotations['is_test_sentence']]

    # drop laws
    law_column = annotations.apply(lambda row: get_law(row), axis=1)
    annotations = annotations.assign(law=law_column.values)
    laws_in_gold_standard = ['Wet_openbaarheid_van_bestuur', 'Rijkswet_op_het_Nederlanderschap']
    annotations = annotations[~annotations['law'].isin(laws_in_gold_standard)]

    return annotations


def remove_duplicate_identical_annotations(annotations):
    # collect sentence_ids that have only one srl_tags sequence associated with them
    temp = annotations.astype({'srl_tags': str})
    same_annotation_ids = []
    for sentence_id in temp['sentence_id'].unique():
        if len(temp.loc[(temp['sentence_id'] == sentence_id)]['srl_tags'].unique()) == 1:
            same_annotation_ids.append(sentence_id)

    # get subset of the annotations that have only one srl_tags sequence associated with them
    same_annotations = annotations[annotations['sentence_id'].isin(same_annotation_ids)]

    # remove this subset from annotations
    annotations = annotations[~annotations['sentence_id'].isin(same_annotation_ids)]

    # remove duplicate annotations from subset
    same_annotations = same_annotations[~same_annotations.duplicated(subset=['sentence_id'], keep='last')]

    # add subset to annotations again
    annotations = pd.concat([annotations, same_annotations])

    return annotations


def get_best_annotations(different_annotations):
    # get annotator ranking
    annotator_performance = pd.read_csv('../resource/evaluation/annotation_2022/annotator_performance.csv')
    annotator_ranking = annotator_performance['user_id'].tolist()

    different_annotations_best = pd.DataFrame()

    # get best annotator for each pair of annotations for a sentence_id
    for sentence_id in different_annotations['sentence_id']:
        different_annotation = different_annotations.loc[(different_annotations['sentence_id'] == sentence_id)]
        user_ids = different_annotation['user_id'].tolist()
        best_annotator = []
        for user_id in user_ids:
            annotator_ranking.index(user_id)
            if not best_annotator:
                best_annotator.append(user_id)
            else:
                if user_id < best_annotator[0]:
                    best_annotator = [user_id]

        # keep only the best annotators
        different_annotation_best = different_annotation[different_annotation['user_id'].isin(best_annotator)]
        different_annotations_best = pd.concat([different_annotations_best, different_annotation_best])

    different_annotations_best = different_annotations_best[~different_annotations_best.duplicated(subset=['sentence_id'], keep='last')]

    return different_annotations_best


def remove_duplicate_different_annotations(annotations):
    # collect sentence_ids that have only one srl_tags sequence associated with them
    temp = annotations.astype({'srl_tags': str})
    different_annotation_ids = []
    for sentence_id in temp['sentence_id'].unique():
        if len(temp.loc[(temp['sentence_id'] == sentence_id)]['srl_tags'].unique()) == 2:
            different_annotation_ids.append(sentence_id)

    # get subset of the annotations that have only one srl_tags sequence associated with them
    different_annotations = annotations[annotations['sentence_id'].isin(different_annotation_ids)]

    # remove this subset from annotations
    annotations = annotations[~annotations['sentence_id'].isin(different_annotation_ids)]

    # remove duplicate annotations from subset
    different_annotations = get_best_annotations(different_annotations)

    # add subset to annotations again
    annotations = pd.concat([annotations, different_annotations])

    return annotations


def remove_annotations_wrong_shape(annotations):
    sentence_ids = []
    counter = 0

    # remove sentence ids where number of tokens is not equal to number of srl_tags  #todo figure out why this happens
    for index, row in annotations.iterrows():
        if len(row.tokens) != len(row.srl_tags):
            counter += 1
            sentence_ids.append(row.sentence_id)
    annotations = annotations[~annotations['sentence_id'].isin(sentence_ids)]

    return annotations


def main():
    annotations = pd.read_csv('../resource/annotations/annotation_2022/all_annotations_merged.csv',
                              converters={'tokens': eval, 'srl_tags': eval},
                              sep=';', encoding='utf-8')

    annotations = remove_unwanted_annotations(annotations)
    annotations = remove_sentences_from_gold_standard(annotations)
    annotations = remove_duplicate_identical_annotations(annotations)
    annotations = remove_duplicate_different_annotations(annotations)
    annotations = remove_annotations_wrong_shape(annotations)

    annotations.to_csv('../resource/annotations/annotation_2022/all_annotations_cleaned.csv',
                       index=False, encoding='utf-8', sep=';')


if __name__ == '__main__':
    main()
