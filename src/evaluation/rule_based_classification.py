"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Maaike de Boer, Roos Bakker
    @contact: maaike.deboer@tno.nl, roos.bakker@tno.nl
"""

import os
import ast
import os
import re
import math
# This script transforms POStagged text to a FLINT frame.
import json
from typing import Tuple

import pandas as pd

action_verbs = ['aanbrengen', 'aanwijzen', 'achterwege blijven', 'afnemen', 'afwijken', 'afwijzen',
                'ambtshalve verlenen', 'ambtshalve verlengen', 'annuleren', 'behandelen', 'beheren', 'bepalen',
                'beperken', 'betreden', 'beveiligen', 'bevelen', 'bevorderen', 'bieden gelegenheid', 'bijhouden',
                'buiten behandeling stellen', 'buiten werking stellen', 'doorzoeken', 'erop wijzen',
                'gebruiken maken van', 'gedwongen ontruimen', 'geven', 'heffen', 'in bewaring stellen',
                'in de gelegenheid stellen zich te doen horen', 'in kennis stellen', 'in werking doen treden',
                'in werking stellen', 'indienen', 'innemen', 'instellen', 'intrekken', 'invorderen', 'inwilligen',
                'maken', 'naar voren brengen', 'nemen', 'niet in behandeling nemen', 'niet-ontvankelijk verklaren',
                'nogmaals verlengen', 'om niet vervoeren', 'onderwerpen', 'onderzoeken', 'ongewenstverklaren',
                'onmiddellijk bepalen', 'onmiddellijk verlaten', 'ontnemen', 'ontvangen', 'opheffen', 'opleggen',
                'oproepen', 'overbrengen', 'overdragen', 'plaatsen', 'schorsen', 'schriftelijk in kennis stellen',
                'schriftelijk laten weten', 'schriftelijk mededelen', 'schriftelijk naar voren brengen', 'signaleren',
                'sluiten', 'staande houden', 'stellen', 'straffen', 'ter hand stellen', 'teruggeven',
                'tijdelijk in bewaring nemen', 'toetsen', 'toezenden', 'uitstellen', 'uitvaardigen', 'uitzetten',
                'van rechtswege verkrijgen', 'vaststellen', 'vergelijken', 'verhalen', 'verhogen', 'verklaren',
                'verkorten', 'verkrijgen', 'verlaten', 'verlenen', 'verlengen', 'verplichten', 'verschaffen',
                'verstrekken', 'verzoeken', 'voegen', 'vorderen', 'vragen', 'willigen', 'weigeren', 'wijzigen']
set_propernouns = ["PRP", "PRP$", "NNP", "NNPS"]

list_act = []


def read_csv_to_df(csv_file):
    datafrm = pd.read_csv(csv_file, delimiter=';')
    print("csv loaded from " + csv_file)
    return datafrm


def write_df_to_csv(df, fle):
    df.to_csv(fle)
    print("df written to " + fle)


def get_object_and_actor(orig, tags) -> Tuple[str, str]:
    vp_found = False
    obj = ""
    actor_num = -1

    # check the index of the verb
    for i in range(0, len(tags)):
        try:
            # find the VP
            if tags[i][0] == "VP" and (tags[i][len(tags[i][0])][0] == orig):
                vp_found = True

            for num in range(1, len(tags[i])):
                # get the first NP; this is the object
                # TODO: version 2: create better code using dependencies to determine the object and actor
                if not vp_found:
                    # bug fix: no lower, because the link to the actor is gone then
                    obj += " " + (str(tags[i][num][0]))
                # only add NPs if they are in the same sentence as the VP of the act
                if "$" in str(tags[i][num][0]) and not vp_found:
                    obj = ""

                # try to find the actor and recipient

                # Hack: make a list of characters and check whether the first is uppercased (capitalized)
                if tags[i][num][1] in set_propernouns and list(tags[i][num][0])[0].isupper() and actor_num < 0:
                    list_non_actors = ['Onderdeel', 'Lid', 'Indien', 'Tenzij', 'Onverminderd', 'Nadat']

                    if not (any(non_actor in tags[i][num][0] for non_actor in list_non_actors)):
                        # print(tags[i][num])
                        actor_num = i
        except:
            # if tags[i][len(tags[i][0])][0] or tags[i][0] does not exist, we have an error
            'do nothing'

    # the actor is the NP of the actor_num (number in the tags)
    actor = ""
    # fixed bug: bigger than -1 if the word occurs as the first word
    if actor_num > -1:
        # range starts with 1, because 0 is the type NP
        for nr in range(1, len(tags[actor_num])):
            actor += " " + tags[actor_num][nr][0]

    # hacks to get a better object
    if len(actor) > 1 and actor in obj:
        obj = obj.replace(actor, "")

    if "kan" in obj:
        obj = obj.replace("kan", "")
    return actor, obj


def check_infinitive(inf, row) -> bool:
    return inf in action_verbs and not \
        ("het " + inf) in row['Brontekst'] or \
           ("de " + inf) in row['Brontekst'] or \
           ("een " + inf) in row['Brontekst']


def get_acts(row, srl_tags) -> list:
    verbs = ast.literal_eval(row['verbs'])
    tags = ast.literal_eval(row['tags'])
    tokens = ast.literal_eval(row['tokens'])

    # for each verb (if one verb this also works)
    for infinitive, original in verbs.items():
        # addition to wrong parsing:
        # acts are not those that have a determiner before it; Dutch determiners are 'de',
        # 'het' and 'een'
        # acts are not those that have 'indien' as a form of 'indienen'
        if check_infinitive(infinitive, row) and not original == 'indien':
            # replace O in srl_tags with V (verb)
            srl_tags[tokens.index(original)] = 'V'

            # get the actor and object
            actor, obj = get_object_and_actor(original, tags)
            # replace in the srl_tags
            if len(actor) > 0:
                actor_parts = re.split('\W+', actor)
                for actor_part in actor_parts:
                    try:
                        srl_tags[tokens.index(actor_part)] = 'ACTOR'
                    except:
                        if len(actor_part) > 1:
                            print(actor_part + ' not found in ' + str(tokens))
            if len(obj) > 0:
                obj_parts = re.split('\W+', obj)
                for obj_part in obj_parts:
                    try:
                        srl_tags[tokens.index(obj_part)] = 'OBJ'
                    except:
                        if len(obj_part) > 1:
                            print(obj_part + ' not found in ' + str(tokens))

            # TODO in version 2: make code better; now only vreemdeling as recipient
            if "vreemdeling" in row['Brontekst']:
                try:
                    srl_tags[tokens.index("vreemdeling")] = 'REC'
                except:
                    print("vreemdeling not found in " + str(tokens))
    return srl_tags


def create_srl_tags(df, name_law):
    """

    :type df: object
    """
    srl_tags_all = []
    # loop through the rows and create acts and facts as we go
    for index, row in df.iterrows():

        if type(row['srl_tags']) == str:
            # if not math.isnan(row['srl_tags']):
            # create srl_tags with all O (letter) in length of the tokens
            srl_tags = ast.literal_eval(row['srl_tags'])

            srl_tags = [re.sub(r'[a-zA-Z]+', 'O', w) for w in srl_tags]

            # Acts: only if we have verbs, we check the srl_tags
            if not "[]" == row['verbs']:
                srl_tags = get_acts(row, srl_tags)
            else:
                'no acts'
        else:
            srl_tags = []
        # add the specific tag to the list of tags
        srl_tags_all.append(srl_tags)

    # add the tags to the dataframe
    df['srl_tags_rule'] = srl_tags_all
    return df


if __name__ == '__main__':
    #csv_file = "all_positives_with_tags_full.csv"  # "Vreemdelingenwet_srl.csv"
    #df = read_csv_to_df(csv_file)
    #df_srl = create_srl_tags(df, 'Vreemdelingenwet')
    #write_df_to_csv(df_srl,
                    #"all_positives_with_tags_full_rulebased.csv")  # "Vreemdelingenwet_srl_rulebased.csv")

    cwd = os.getcwd()
    csv_file = os.path.join(cwd, "src", "resource", "to_be_labeled", "ground_truth_postagged_pattern.csv")
    df = read_csv_to_df(csv_file)
    #df = df.drop(columns=['srl_tags'])
    df_srl = create_srl_tags(df, 'Vreemdelingenwet')
    df_srl.to_csv(os.path.join(cwd, "src", "resource", "labeled_results",
                               "predicted_labels_groundtruth_rulebased_old.csv"), sep=";")
#     method = "TOGS"
#     base = 'C:\\Users\\boermhtd\\PycharmProjects\\calculemus\\nlp\\data\\csv_files\\postagged\\'
#     if method == "TOGS":
#         csv_file = base + 'BWBR0043324_2020-03-31_0_TOGS_postagged.csv'
#     elif method == "TOZO":
#         csv_file = base + 'BWBR0043402_2020-04-22_0_TOZO_postagged.csv'
#     elif method == "AWB":
#         csv_file = base + 'BWBR0005537_2020-04-15_0_AWB_postagged.csv'
#
#                #'BWBR0011823_2019-02-27_Vreemdelingenwet_postagged.csv'
#
#     output_file = method + '_new.json'
#     dataframe_to_frame_parser(csv_file, output_file)
#
#     act_file = "acts_" + method + ".csv"
#     df_act = pd.DataFrame(list_act, columns = ['action', 'sentence'])
#     df_act.to_csv(act_file, index=False)
#
#     fact_file = "facts_" + method + ".csv"
#     df_fact = pd.DataFrame(list_fact, columns = ['fact', 'definition'])
#     df_fact.to_csv(fact_file, index=False)

#     # df = read_csv_to_df(str(csv_file))
#     flint_frames = create_flint_frames(df)
#     write_flint_frames_to_json(flint_frames)
