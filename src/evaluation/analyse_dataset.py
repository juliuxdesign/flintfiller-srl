import pandas as pd


def count_number_of_annotations(annotations, no_of_annotations, save=False):
    print('NUMBER OF ANNOTATIONS PER SENTENCE')
    sentence_id_counts = annotations['sentence_id'].value_counts().rename_axis('sentence_id').reset_index(name='counts')
    print(len(sentence_id_counts), 'sentences have been annotated')
    print(sentence_id_counts.groupby(['counts'])['counts'].count())

    if save:
        # save a csv that contains only the annotations that have been annotated x times (x=no_of_annotations, e.g. 1)
        annotations.to_csv('../resource/evaluation/annotation_2022/sentences_annotated_once.csv',
                           index=False)


def count_number_of_different_same_annotations(annotations, no_of_annotations=2, save=False):
    # On how many sentences that are annotated twice do the annotators not agree? On how many do they agree?
    annotations = annotations.astype({"srl_tags": str})
    different_annotations = []
    same_annotations = []
    for sentence_id in annotations['sentence_id'].unique():
        if len(annotations.loc[(annotations['sentence_id'] == sentence_id)]['srl_tags'].unique()) == 1:
            same_annotations.append(sentence_id)
        elif len(annotations.loc[(annotations['sentence_id'] == sentence_id)]['srl_tags'].unique()) == 2:
            different_annotations.append(sentence_id)

    print('...', len(different_annotations), 'of which have been annotated differently for the 2 different annotators')
    print('...', len(same_annotations), 'of which have been annotated identically for the 2 different annotators')

    if save:
        annotations[annotations['sentence_id'].isin(different_annotations)].to_csv('../resource/evaluation/'
                                                                                   'annotation_2022/'
                                                                                   'sentences_annotated_differently.csv',
                                                                                   index=False)
        annotations[annotations['sentence_id'].isin(same_annotations)].to_csv('../resource/evaluation/annotation_2022/'
                                                                              'sentences_annotated_identically.csv',
                                                                              index=False)


def get_value_counts(annotations_df):
    df = pd.DataFrame(annotations_df)
    df['temp~'] = df['sentence_id'].str.split('-')
    df['sent_id'] = df['temp~'].str.get(1)
    df = df.drop(columns=['temp~'])
    return df['sent_id'].value_counts()


def create_law_overview(save=False):
    annotations_once = pd.read_csv('../resource/evaluation/annotation_2022/sentences_annotated_once.csv')
    annotations_differently = pd.read_csv('../resource/evaluation/annotation_2022/sentences_annotated_differently.csv')
    annotations_id = pd.read_csv('../resource/evaluation/annotation_2022/sentences_annotated_identically.csv')
    df_value_counts_id = get_value_counts(annotations_id)
    df_value_counts_once = get_value_counts(annotations_once)
    df_value_counts_differently = get_value_counts(annotations_differently)
    df_val_count = pd.DataFrame(
        {'id': df_value_counts_id, 'once': df_value_counts_once, 'diff': df_value_counts_differently})
    if save:
        df_val_count.to_csv('value_counts_per_law.csv')


def create_ranking_annotator_performance():
    # note: the colorcoded csv does not contain all sentences that have been annotated differently
    annotations_diff_all = pd.read_csv('../resource/evaluation/annotation_2022/sentences_annotated_differently.csv')

    # count for each annotator (how many annotations of them are even in this set?)
    print(annotations_diff_all['user_id'].value_counts())

    annotations_different = pd.read_csv('../resource/evaluation/annotation_2022'
                                        '/sentences_annotated_differently_colorcoded.csv', sep=';')

    # count for each annotator (how many annotations of them were annotated differently)
    print(annotations_different['user_id'].value_counts())

    annotations_different = annotations_different[annotations_different['color_coded'].notna()]
    annotations_different = annotations_different[annotations_different['color_coded'].isin(['green', 'orange', 'red'])]

    # get counts for each color (per annotator)
    counts_per_color = annotations_different.groupby(['user_id', 'color_coded'])[['tokens']].count().unstack()
    counts_per_color = counts_per_color.droplevel(level=0, axis=1)
    counts_per_color = counts_per_color.reset_index()
    counts_per_color = counts_per_color.fillna({'green': 0, 'orange': 0, 'red': 0})
    cols = ['green', 'orange', 'red']
    counts_per_color[cols] = counts_per_color[cols].apply(pd.to_numeric, downcast='integer', errors='ignore')

    # get total counts (per annotator)
    counts_total = annotations_different.groupby(['user_id'])['tokens'].count().reset_index()
    counts_total = counts_total.rename(columns={'tokens': 'total'})

    # define which colors are defined as correct
    counts_per_color['correct'] = counts_per_color['green'] + counts_per_color['orange']
    counts_per_color['incorrect'] = counts_per_color['red']

    # get relative counts
    counts = pd.merge(left=counts_per_color, right=counts_total, how='outer')
    counts['correct_relative'] = counts['correct'] / counts['total']

    # sort by how many relatively correct (and no of green secondary) to obtain annotator ranking
    counts = counts.sort_values(by=['correct_relative', 'green'], ascending=False)
    counts.to_csv('../resource/evaluation/annotation_2022/annotator_performance.csv', index=False)


def main():
    annotations = pd.read_csv('../resource/annotations/annotation_2022/all_annotations_merged.csv',
                              converters={'tokens': eval, 'srl_tags': eval},
                              sep=';', encoding='utf-8')

    count_number_of_annotations(annotations, no_of_annotations=1, save=False)
    count_number_of_different_same_annotations(annotations, no_of_annotations=2, save=False)
    create_law_overview(save=False)
    create_ranking_annotator_performance()


if __name__ == '__main__':
    main()



