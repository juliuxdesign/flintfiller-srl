import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from metrics import get_accuracy
cwd = os.getcwd()


def format_bert_predictions(pred_output):
    """
    Code re-used from Romy's notebook: removes BERT tokens such as [CLS] or [SEP] and combines tokens that are split
    by the wordpiece algorithm.
    Args:
        pred_output: list of tokens with labels.

    Returns: reformatted output.

    """
    # remove CLS and SEP token at start and end of list
    pred_output = pred_output[1:-1]

    # concatenate the tokens that start with ## to their preceding token
    cleaned_pred_output = []
    for label in pred_output:
        label = list(label)
        if not label[0].startswith('##'):
            cleaned_pred_output.append(label)
        else:
            cleaned_pred_output[-1][0] += label[0].strip('##')

    #Map the different usages of arguments
    label_mapping = {'ACTOR': 'Actor',
                     'OBJ': 'Object',
                     'REC': 'Recipient',
                     'V': 'Action',
                     'O': 'O'}

    for label in cleaned_pred_output:
        label[1] = label_mapping[label[1]]

    cleaned_pred_output = [tuple(label) for label in cleaned_pred_output]

    return cleaned_pred_output


def read_rlb_dataframe(path_to_rlb: str =
                        os.path.join(cwd, "src", "resource", "labeled_results", "ground_truth_incl_sentence_id.csv")):
    rlb_df = pd.read_csv(path_to_rlb, sep=";", converters={'srl_tags_rulebased': eval})
    rlb_df = rlb_df[['sentence_id', 'srl_tags_rulebased']]
    return rlb_df


def read_srl_dataframe(path_to_srl: str =
                       os.path.join(cwd, "src", "resource", "labeled_results",
                                    "predicted_labels_ground_truth_incl_prob.csv")):
    srl_df = pd.read_csv(path_to_srl, sep=",", converters={'predicted_labels': eval})
    srl_df = srl_df.rename(columns={'predicted_labels': 'srl_tags_BERT'})
    # gebruik code van Romy: maak de labels gelijk: door [CLS] en ## tokens van bert zijn de lijsten met tokens niet meer gelijk
    srl_df['srl_tags_BERT'] = srl_df['srl_tags_BERT'].apply(format_bert_predictions)
    srl_df = srl_df[["sentence_id", "srl_tags_BERT"]]
    return srl_df


def read_ground_truth_dataframe(path_to_ground_truth: str =
                                os.path.join(cwd, "src", "resource", "to_be_labeled", "ground_truth.csv")):
    true_labels = pd.read_csv(path_to_ground_truth, delimiter=';', converters={'tokens': eval, 'srl_tags': eval})
    true_labels = true_labels[["sentence_id", "Brontekst", "tokens", "srl_tags"]]
    return true_labels


def read_dataframes():
    rlb_df = read_rlb_dataframe()
    srl_df = read_srl_dataframe()
    merged = rlb_df.merge(srl_df, left_on='sentence_id', right_on='sentence_id', how='left')
    assert(len(merged)) == len(srl_df), "dataframes rowlength should match"

    # Sanity check: are the rulebased tokens and srl tokens of the same length
    for x, z in zip(list(merged['srl_tags_rulebased']), list(merged['srl_tags_BERT'])):
        assert len(x) == len(z), "lengths should match"
    return merged

# option one: when there is no action in srl output, select the rulebased action

def find_sentences_without_action(merged_df: pd.DataFrame):
    """
    Add a column that specifies whether the sentence has an action label or not according to the SRL
    Args:
        merged_df: combined srl & rlb dataframe

    Returns: merged_df with a column that specifies if there are 0 tokens with an action in the sentence as False,
    othewise True. Also return 2 dataframes with all rows with and without an action
    """
    action_in_srl_lst = []
    for rlb_output, srl_output in zip(list(merged_df['srl_tags_rulebased']), list(merged_df['srl_tags_BERT'])):
        actions = [label for token, label, prob in srl_output if label == 'Action']
        # check if there is an action in the list
        if len(actions) == 0:
            action_in_srl = False
        else:
            action_in_srl = True
        action_in_srl_lst.append(action_in_srl)

    print(f"total occurences of no actions in srl is: {len([x for x in action_in_srl_lst if x == False])}")
    merged_df['srl_action_avaialble'] = action_in_srl_lst
    merged_wo_action = merged_df.loc[merged_df['srl_action_avaialble'] == False]
    merged_with_action = merged_df.drop(merged_df[merged_df.srl_action_avaialble == False].index)
    assert action_in_srl_lst.count(False) == 8, "there should be 8 occurences of no action in SRL"
    return merged_df, merged_wo_action, merged_with_action


def remove_probability(df: pd.DataFrame):
    """
    Reconstructs the input from [(token, label, probability), ... ] to [(token, label), ..]
    Args:
        df:

    Returns: dataframe with reconstructed input.

    """
    reconstructed_sents = []
    for sent in list(df['srl_tags_BERT']):
        reconstructed_sent = []
        for token, label, prob in sent:
            reconstructed_token = (token, label)
            reconstructed_sent.append(reconstructed_token)
        reconstructed_sents.append(reconstructed_sent)

    df = df.drop(columns=['srl_tags_BERT'])
    df['srl_tags_BERT'] = reconstructed_sents
    return df


def insert_rlb_action_sentences(merged_with_action, merged_without_action):
    """
    Select the sentences without an action according to the SRL. Read in the ground_truth.csv sentences and select just
    the sentence_ids that are again without action. Loop through 1) the rlb_output, 2) srl_output and 3) ground truth
    output. Each time an action token is encountered in the rulebased, add the action label to the reconstructed sents.
    Args:
        merged_with_action: sliced SRL dataframe with all sentences that contain atleast one action according to SRL
        merged_without_action: sliced SRL dataframe with all sentences that do not contain an action according to SRL

    Returns: Reconstructed dataframe with the action labels from the RLB and the other labels from the SRL.

    """
    # out of curiousity: store the probabilities of the tokens that the RLB labels as action but that the SRL does not

    # import ground truth for cmparison
    true_labels = read_ground_truth_dataframe()

    # only select the ground truth labels ofthe sentences without action
    true_labels = true_labels.loc[true_labels['sentence_id'].isin(list(merged_without_action['sentence_id']))]
    true_labels = true_labels.sort_values(by="sentence_id")
    merged_without_action = merged_without_action.sort_values(by="sentence_id")
    assert all([x == y for x, y in zip(list(merged_without_action['sentence_id']), list(true_labels['sentence_id']))]), "the sentence ids should match"
    store_probabilities_actions = []
    reconstruct_sentences = []
    for sentence_id, rlb_output, srl_output, ground_truth_output, ground_truth_tokens in zip(list(merged_without_action['sentence_id']),
                                                                       list(merged_without_action['srl_tags_rulebased']),
                                                                       list(merged_without_action['srl_tags_BERT']),
                                                                       list(true_labels['srl_tags']),
                                                                       list(true_labels['tokens'])):
        reconstruct_sentence = []
        # each tuple is (token, label), the srl tuple also has the probablity of the prediction
        # whenever we encounter the rlb action we select this label, otherwise the other label is selected
        for rlb_tuple, srl_tuple, ground_truth_label, ground_truth_token in zip(rlb_output, srl_output, ground_truth_output,
                                                            ground_truth_tokens):
            if rlb_tuple[1] == "action":
                format_output = (srl_tuple[0], "Action")
                reconstruct_sentence.append(format_output)
                store_probabilities_actions.append(srl_tuple[2])
            else:
                format_output = (srl_tuple[0], srl_tuple[1])
                reconstruct_sentence.append(format_output)
        reconstruct_sentences.append(reconstruct_sentence)
    assert len(reconstruct_sentences) == len(merged_without_action), "lengths should be the same"
    print(f"the average probability for actions that the SRL ignored is: "
          f"{sum(store_probabilities_actions)/len(store_probabilities_actions)}")

    # drop the srl_tags_BERT and replace these with the new reconstructed sentences
    merged_without_action = merged_without_action.drop(columns=['srl_tags_BERT'])
    merged_without_action['srl_tags_BERT'] = reconstruct_sentences
    # merge the dataframes again with the new
    merged_with_action = remove_probability(merged_with_action)
    merged_df = pd.concat([merged_with_action, merged_without_action])
    return merged_df, merged_without_action


def main_option_one():
    """
    Read in the input dataframes, select the sentences without an action according to SRL and insert the RLB token
    into these sentences. Store the output in the "labeled_results" folder.
    Returns:

    """
    merged = read_dataframes()
    merged, merged_wo_action, merged_with_action = find_sentences_without_action(merged)
    merged_incl_rlb_action_sents, merged_without_action = insert_rlb_action_sentences(merged_with_action,
                                                                                      merged_wo_action)

    merged_incl_rlb_action_sents.to_csv(
        os.path.join(cwd, "src", "resource", "labeled_results", "predicted_labels_hybrid_srl_rlb_option_one.csv"),
        sep=";")
    return merged_incl_rlb_action_sents


merged_incl_rlb_action_sents = main_option_one()

# option two:


def retrieve_probabilities(srl_dataframe):
    store_probabilities = []
    for sent in list(srl_dataframe['srl_tags_BERT']):
        for token, label, confidence in sent:
            store_probabilities.append(confidence)

    plt.title("originele distributie van probabilities")
    plt.hist(store_probabilities)
    print(f"minimum probability is: {min(store_probabilities)}")
    return store_probabilities


def refactor_probabilities(probability_list):
    """takes the list of numbers and maps them
    to a different (more human-readable) scale."""
    newresults = []
    for number in probability_list:
        #newresults.append(np.interp(number, [min(store_probabilities), 1], [0, 1]))
        newresults.append(np.interp(number, [0.99, 1], [0, 1]))
    return newresults


def transform_probabilities(srl_dataframe):
    """
    Transform probabilities using numpy interp.
    Args:
        srl_dataframe:

    Returns:

    """
    reconstructed_sents = []
    for sent in list(srl_dataframe['srl_tags_BERT']):
        reconstructed_sent = []
        for token, label, prob in sent:
            reconstructed_tuple = (token, label, np.interp(prob, [0.99, 1], [0, 1]))
            reconstructed_sent.append(reconstructed_tuple)
        reconstructed_sents.append(reconstructed_sent)
    srl_dataframe = srl_dataframe.drop(columns=['srl_tags_BERT'])
    srl_dataframe['srl_tags_BERT'] = reconstructed_sents
    return srl_dataframe


def evaluate_thresholds(srl_df, rlb_df, hist_plot):
    """
    Use the bins extracted from the histogram as confidence cut-off points, i.e. [0.05, 0.10, 0.15, .. , 1]. Loop
    over each bin and the rlb_output and srl_output. Whenever the confidence of a BERT token prediction is lower than
    the confidence cutoff point, select the RLB label otherwise select the SRL label.
    Args:
        srl_df:
        rlb_df:
        hist_plot:

    Returns:

    """
    store_thresholds = []
    store_accuracies = []
    # loop over the histogram bin cut off points
    for cutoff_point in hist_plot[1]:
        reconstructed_sents = []
        for sentence_id, rlb_output, srl_output, in zip(list(srl_df['sentence_id']),
                                                        list(rlb_df['srl_tags_rulebased']),
                                                        list(srl_df['srl_tags_BERT'])):
            reconstructed_sent = []
            for rlb_tuple, srl_tuple in zip(rlb_output, srl_output):
                if srl_tuple[2] < cutoff_point:
                    # maybe Title voor hoofdletters voor label
                    reconstructed_tuple = (srl_tuple[0], rlb_tuple[1].title())
                    reconstructed_sent.append(reconstructed_tuple)
                else:
                    reconstructed_sent.append((srl_tuple[0], srl_tuple[1]))
            reconstructed_sents.append(reconstructed_sent)

        result_df = srl_df.drop(columns=['srl_tags_BERT'])
        result_df['srl_tags_BERT'] = reconstructed_sents
        true_labels = read_ground_truth_dataframe()
        df = pd.merge(left=result_df, right=true_labels, on='sentence_id', how='inner')

        mean_accuracy = get_accuracy(df, "bert")
        store_thresholds.append(cutoff_point)
        store_accuracies.append(mean_accuracy)

    plt.title("confidence versus accuracy: select RLB when confidence is below threshold")
    plt.xlabel("Confidence threshold")
    plt.ylabel("Mean accuracy")
    # performance SRL
    plt.axhline(y=0.8425, color='r', linestyle='-')
    plt.ylim([0, 1])
    plt.plot(store_thresholds, store_accuracies)


def main_option_two():
    """
    Read in the input dataframes and refactor the probabilities. Evaluate 20 different confidence values and compare
    these with the BERT thresholds. Whenever the BERT confidence is lower than the current threshold, insert the RLB
    label.
    Returns:

    """
    srl_df = read_srl_dataframe()
    rlb_df = read_rlb_dataframe()
    store_probabilities = retrieve_probabilities(srl_df)
    reformatted_probabilities = refactor_probabilities(store_probabilities)
    plt.title("reformatted probabilities: 0.99-1")
    hist_plot = plt.hist(reformatted_probabilities, bins=20)
    srl_df = transform_probabilities(srl_df)

    evaluate_thresholds(srl_df, rlb_df, hist_plot)


# Wat is het verschil tussen de gemiddelde ground truth probability als de SRL goed- en fout voorspeld?
def calculate_mean_probability():
    srl_df = read_srl_dataframe()
    ground_truth_df = read_ground_truth_dataframe()
    ground_truth_df = ground_truth_df.sort_values(by="sentence_id")
    srl_df = srl_df.sort_values(by="sentence_id")

    store_equal_probabilities = []
    store_unequal_probabilities = []

    for srl_output, ground_truth_output in zip(list(srl_df['srl_tags_BERT']), list(ground_truth_df['srl_tags'])):
        for srl_tuple, ground_truth_label in zip(srl_output, ground_truth_output):
            if srl_tuple[1] == ground_truth_label:
                store_equal_probabilities.append(srl_tuple[2])
            else:
                store_unequal_probabilities.append(srl_tuple[2])

    print(f"Average probability when SRL labels correctly: {sum(store_equal_probabilities)/len(store_equal_probabilities)}")
    print(f"Average probability when SRL labels incorrectly: "
          f"{sum(store_unequal_probabilities)/len(store_unequal_probabilities)}")


if __name__ == '__main__':
    main_option_one()
    main_option_two()
    calculate_mean_probability()
