import pandas as pd

# NOTE do not push this to open source repo (privacy. names of annotators)
# todo anonymize

from evaluation.calculate_agreement import preprocess_annotations, drop_incomplete_annotations, format_annotations, \
    calculate_fleiss_kappa

if __name__ == '__main__':

    control_sentences_ground_truth = pd.read_csv('../resource/annotations/annotation_2022/control_sentences.csv',
                                                 converters={'tokens': eval, 'srl_tags': eval},
                                                 encoding='ANSI', sep=";")

    annotations = pd.read_csv('../resource/annotations/annotation_2022/annotation_external_round1.csv',
                              converters={'tokens': eval, 'srl_tags': eval, 'timestamps': eval},
                              encoding='ANSI', sep=",")

    control_sentences = annotations[annotations['is_test_sentence'] == True]

    control_sentences = control_sentences.drop(columns=['is_test_sentence', 'comments', 'timestamps'])

    control_sentences_m = control_sentences[control_sentences['user_id'] == "Manoukbea"]
    control_sentences_a = control_sentences[control_sentences['user_id'] == "Anthoinettehelm"]
    control_sentences_d = control_sentences[control_sentences['user_id'] == "Davidthies"]
    control_sentences_r = control_sentences[control_sentences['user_id'] == "Roosvandert"]

    m = control_sentences_m.append(control_sentences_ground_truth, ignore_index=True)
    a = control_sentences_a.append(control_sentences_ground_truth, ignore_index=True)
    d = control_sentences_d.append(control_sentences_ground_truth, ignore_index=True)
    r = control_sentences_r.append(control_sentences_ground_truth, ignore_index=True)

    for annotation in [m, a, d, r]:
        print(annotation['user_id'][0])
        annotation = drop_incomplete_annotations(annotation, no_of_annotations=2)
        annotation = preprocess_annotations(annotation)
        annotation = format_annotations(annotation)
        kappa = calculate_fleiss_kappa(annotation)
        print(kappa)
        print()


