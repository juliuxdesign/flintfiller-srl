from sklearn.metrics import accuracy_score, confusion_matrix

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import itertools
import ast
import string

from ast import literal_eval


def read_csv_to_df(csv_file):
    datafrm = pd.read_csv(csv_file, delimiter=';')
    print("csv loaded from " + csv_file)
    return datafrm


# code to plot the confusion matrix
# inspired / taken from: http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    # print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def remove_punctuation(list_y):
    list_y = [word for word in list_y if word not in set(string.punctuation)]
    return list_y


def show_confusion_matrix(conf_matrix_list_of_arrays, class_names):
    mean_of_conf_matrix_arrays = np.mean(conf_matrix_list_of_arrays, axis=0)
    # mean_of_conf_matrix_arrays *= 1000
    print(mean_of_conf_matrix_arrays.tolist())
    plt.figure()
    plot_confusion_matrix(mean_of_conf_matrix_arrays, classes=class_names, title='Mean confusion matrix',
                          normalize=True)
    plt.show()


def get_accuracy(df, type_method):
    mean_accuracy = 0.0
    conf_matrix_list_of_arrays = []
    for index, row in df.iterrows():
        # for the rule based method the predictions are correct
        if type_method == "rule":
            y_pred = ast.literal_eval(row['srl_tags_rule'])
        # for the bert the predictions some parts have to be deleted and the labels have to be extracted
        elif type_method == "bert":
            # y_pred_orig = ast.literal_eval(row['srl_tags_BERT'])
            y_pred = []
            # for y in ast.literal_eval(row['srl_tags_BERT']):
            for y in row['srl_tags_BERT']:
                y_pred.append(y[1])
                # if "CLS" not in y[0] and "SEP" not in y[0] and "#" not in y[0] and y[0] not in set(string.punctuation):
                #     # get only the label
                #     y_pred.append(y[1])

        # y_true = ast.literal_eval(row['srl_tags'])
        y_true = row['srl_tags']
        # print('y_true: ground truth: \n', y_true, len(y_true))
        # print('\n')
        # print('y_pred: \n', y_pred , len(y_pred))
        # y_true = remove_punctuation(y_true) # TODO figure out how to remove punctuation fairly (commented out for now because punctuation could not be removed for both true and pred, leading to differences in length)
        # y_pred = remove_punctuation(y_pred)

        # TODO: bug fix: only check the positives; set to -1 if you want to check all
        if len(y_true) > -1:
            # this is to calculate performance on empty lists: add one ['O'] for the empty list
            if len(y_true) < 1 and not len(y_pred) < 1:
                y_true = ['O'] * len(y_pred)
            # bug fix: if rule based is empty list
            elif len(y_pred) < 1 and not len(y_true) < 1:
                y_pred = ['O'] * len(y_true)
            elif len(y_pred) < 1 and len(y_true) < 1:
                y_pred = ['O']
                y_true = ['O']

            # this is a bug fix if needed
            if len(y_true) != len(y_pred):
                print('y_true', len(y_true), y_true)
                print('y_pred', len(y_pred), y_pred)
                print(row['tokens'])
                continue

            accuracy = accuracy_score(y_true, y_pred)
            # print(accuracy)
            # print the confusion matrix per run
            np.set_printoptions(precision=2)
            # class_names = ['ACTOR', 'O', 'OBJ', 'REC', 'V']
            class_names = ['Actor', 'O', 'Object', 'Recipient', 'Action']
            cm = confusion_matrix(y_true, y_pred, labels=class_names)

            conf_matrix_list_of_arrays.append(cm)
            mean_accuracy += accuracy

    mean_accuracy /= len(conf_matrix_list_of_arrays)
    # this is to show the number of items we have evaluated on
    # print("number of items: ")
    # print(len(conf_matrix_list_of_arrays))
    show_confusion_matrix(conf_matrix_list_of_arrays, class_names)
    return mean_accuracy


def format_bert_predictions(pred_output):
    # remove CLS and SEP token at start and end of list
    pred_output = pred_output[1:-1]

    # concatenate the tokens that start with ## to their preceding token
    cleaned_pred_output = []
    for label in pred_output:
        label = list(label)
        if not label[0].startswith('##'):
            cleaned_pred_output.append(label)
        else:
            cleaned_pred_output[-1][0] += label[0].strip('##')

    # Map the different usages of arguments
    label_mapping = {'ACTOR': 'Actor',
                     'OBJ': 'Object',
                     'REC': 'Recipient',
                     'V': 'Action',
                     'O': 'O'}

    for label in cleaned_pred_output:
        label[1] = label_mapping[label[1]]

    cleaned_pred_output = [tuple(label) for label in cleaned_pred_output]

    return cleaned_pred_output


def create_df_all_predictions(path_srl, path_rlb, path_hybrid, path_rlb_old, path_gt):
    # add predicted labels from 2022 SRL
    pred_labels = pd.read_csv(path_srl, delimiter=',', converters={'predicted_labels': eval})
    pred_labels = pred_labels.rename(columns={'predicted_labels': 'srl_tags_BERT'})

    # do formatting on pred_labels (remove all BERT-specific tokens)
    pred_labels['srl_tags_BERT'] = pred_labels['srl_tags_BERT'].apply(format_bert_predictions)

    # 2022 RLB option one
    pred_labels_option_one = pd.read_csv(path_hybrid, delimiter=';', converters={'srl_tags_BERT': eval})
    # pred_labels = pred_labels.rename(columns={'srl_tags_BERT': 'srl_tags_BERT'})

    # add predicted labels from the new RuleBased (RLB) method
    # (note: the column is renamed to "srl_tags_BERT" because the script expects this specific column name
    pred_labels_rlb = pd.read_csv(path_rlb, delimiter=';', converters={'predicted_labels': eval})
    pred_labels_rlb['srl_tags_BERT'] = [literal_eval(x) for x in pred_labels_rlb['srl_tags_rulebased']]
    pred_labels_rlb = pred_labels_rlb[["sentence_id", "Brontekst", "tokens", "srl_tags_BERT"]]

    # add predicted labels from the old rulebased method
    pred_labels_rlb_2021 = pd.read_csv(path_rlb_old, delimiter=";", converters={'srl_tags_rule': eval})

    # Make all labels uniform in Flint format
    srl_tags_rulebased_old = []
    for list_of_tokens, list_of_labels in zip(list(pred_labels_rlb_2021['tokens']),
                                              pred_labels_rlb_2021['srl_tags_rule']):
        list_of_tokens = literal_eval(list_of_tokens)
        token_label_lst = []
        for token, label in zip(list_of_tokens, list_of_labels):
            if label == "OBJ":
                label = "Object"
            elif label == "ACTOR":
                label = "Actor"
            elif label == "V":
                label = "Action"
            # label equals "O"
            else:
                pass
            token_label_tuple = (token, label)
            token_label_lst.append(token_label_tuple)
        srl_tags_rulebased_old.append(token_label_lst)
    pred_labels_rlb_2021['srl_tags_BERT'] = srl_tags_rulebased_old
    pred_labels_rlb_2021 = pred_labels_rlb_2021[["sentence_id", "Brontekst", "tokens", "srl_tags_BERT"]]
    pred_labels_rlb_2021 = pred_labels_rlb_2021.rename(columns={'srl_tags_rule': 'srl_tags_BERT'})
    pred_labels_rlb_2021 = pred_labels_rlb_2021[["sentence_id", "Brontekst", "tokens", "srl_tags_BERT"]]

    # add ground truth labels to df
    true_labels = pd.read_csv(path_gt, delimiter=';',
                              converters={'tokens': eval, 'srl_tags': eval})
    true_labels = true_labels[["sentence_id", "Brontekst", "tokens", "srl_tags"]]
    df = pd.merge(left=pred_labels, right=true_labels, on='sentence_id', how='inner')
    df_rlb = pd.merge(left=pred_labels_rlb, right=true_labels, on='sentence_id', how='inner')

    # combination option one
    df_rlb_srl = pd.merge(left=pred_labels_option_one, right=true_labels, on='sentence_id', how='inner')
    df_rlb_old = pd.merge(left=pred_labels_rlb_2021, right=true_labels, on='sentence_id', how='inner')
    return df, df_rlb, df_rlb_srl, df_rlb_old


def calculate_accuracies(df, df_rlb, df_rlb_srl, df_rlb_old):
    # Calculate mean accuracies
    mean_accuracy = get_accuracy(df, "bert")
    mean_accuracy_rlb = get_accuracy(df_rlb, "bert")
    mean_accuracy_srl_rlb = get_accuracy(df_rlb_srl, "bert")
    mean_accuracy_rlb_old = get_accuracy(df_rlb_old, "bert")

    # Print accuracy scores
    print("mean accuracy bert: " + str(mean_accuracy))
    print(f"mean accuracy rulebased method is {mean_accuracy_rlb}")
    print(f"mean accuracy srl + rulebased method is {mean_accuracy_srl_rlb}")
    print(f"mean accuracy old rulebased method is {mean_accuracy_rlb_old}")

    return [mean_accuracy, mean_accuracy_rlb, mean_accuracy_srl_rlb, mean_accuracy_rlb_old]


if __name__ == '__main__':
    df, df_rlb, df_rlb_srl, df_rlb_old = create_df_all_predictions(
        "src/resource/labeled_results/predicted_labels_bertje2022_groundtruth.csv",
        "src/resource/labeled_results/predicted_labels_groundtruth_rulebased.csv",
        "src/resource/labeled_results/predicted_labels_hybrid_srl_rlb_option_one.csv",
        "src/resource/labeled_results/predicted_labels_groundtruth_rulebased_old.csv",
        "src/resource/to_be_labeled/ground_truth.csv")

    calculate_accuracies(df, df_rlb, df_rlb_srl, df_rlb_old)
