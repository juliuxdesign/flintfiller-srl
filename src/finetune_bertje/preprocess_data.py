import argparse
import pathlib

import pandas as pd
from datasets import Dataset, DatasetDict
from transformers import AutoTokenizer
import pathlib
import string

directory = pathlib.Path(__file__).parent.parent


def drop_rows(df):
    """
    Remove rows where the number of tokens is not equal to the number of semantic role tags.

    Parameters:
        df:     the input df

    Returns:
        df:     the df without incorrect rows
    """

    indices_to_remove = [index for index, row in df.iterrows() if len(row['tokens']) != len(row['srl_tags'])]
    df = df.drop(indices_to_remove)

    return df


def determine_set_of_desired_srl_tags(df):
    tags_in_df = df['srl_tags'].iloc[0]
    desired_srl_tags_1 = ['REC', 'OBJ', 'ACTOR', 'V']
    desired_srl_tags_2 = ['Recipient', 'Object', 'Actor', 'Action']

    # If tags_in_df contains any elements of the first set of tags, return that set of tags, otherwise return the other
    if any(tag in tags_in_df for tag in desired_srl_tags_1):
        desired_srl_tags_1.append('O')
        return desired_srl_tags_1
    else:
        desired_srl_tags_2.append('O')
        return desired_srl_tags_2


def replace_unknown_tags(df, desired_srl_tags):
    """
    Replace unknown tags (tags that are not V, ACTOR, OBJ, REC or O) with O.

    Parameters:
        df:     the input df
        desired_srl_tags: the desired srl tags

    Returns:
        df:     the df without unknown tags
    """

    # Change tags that are not V, ACTOR, OBJ, REC or O to O
    set_of_desired_tags = set(desired_srl_tags)
    new_srl_tags = []
    for index, row in df.iterrows():
        new_srl_tags.append(['O' if x not in set_of_desired_tags else x for x in row['srl_tags']])
    df = df.drop(columns=['srl_tags'])
    df['srl_tags'] = new_srl_tags

    return df


def encode_srl_tags(df, desired_srl_tags):
    """
    Encode SRL tags as numbers. O --> 0, V --> 1, ACTOR --> 2, OBJ --> 3, REC --> 4.

    Parameters:
        df:     the input df
        desired_srl_tags: the desired srl tags

    Returns:
        df:     the df with an extra column containing numbers for SRL tags.
    """

    # Create new column with number for srl tags instead of text
    if desired_srl_tags == ['Recipient', 'Object', 'Actor', 'Action', 'O']:
        srl_keys = {"O": 0,
                    "Action": 1,
                    "Actor": 2,
                    "Object": 3,
                    "Recipient": 4}
    elif desired_srl_tags == ['REC', 'OBJ', 'ACTOR', 'V', 'O']:
        srl_keys = {"O": 0,
                    "V": 1,
                    "ACTOR": 2,
                    "OBJ": 3,
                    "REC": 4}
    df.rename(columns={'srl_tags': 'srl_tags_name'}, inplace=True)
    srl_tags_numbers = []
    for index, row in df.iterrows():
        srl_tags_numbers.append([srl_keys.get(item) for item in row['srl_tags_name']])
    df['srl_tags'] = srl_tags_numbers

    return df


def create_dataset_object(df):
    """
    Create a Dataset object using a dataframe as input

    Parameters:
        df:         the input df

    Returns:
        datasets:   a DatasetDict object containing 3 Dataset objects
    """

    dataset = Dataset.from_pandas(df)

    train_testvalid = dataset.train_test_split(test_size=0.1)  # 90% train, 10% test + validation
    test_valid = train_testvalid['test'].train_test_split(test_size=0.5)  # 10% test + valid --> 50% test, 50% valid

    # collect all Datasets in a single DatasetDict
    datasets = DatasetDict({
        'train': train_testvalid['train'],
        'test': test_valid['test'],
        'validation': test_valid['train']})

    return datasets


def tokenize_and_align_labels(datasets, label_all_tokens=True):
    """
    Tokenizes the input sentences, and aligns the labels (e.g. actor label, or [CLS].
    Alignment is necessary because the input ids returned by the tokenizer are longer than the lists of labels
    our dataset contain (due to [CLS] and [SEP], and also because some words are split into multiple tokens).
    Special tokens have a word id that is None. We set their label to -100 so they are automatically ignored in the
    loss function.

    Parameters:
        datasets:           a DatasetDict object containing 3 Dataset objects
        label_all_tokens:

    Returns:
        tokenized_input:    the tokenized version of the input
    """
    tokenizer = AutoTokenizer.from_pretrained("GroNLP/bert-base-dutch-cased")
    tokenized_inputs = tokenizer(datasets["tokens"], truncation=True, is_split_into_words=True)

    labels = []
    for i, label in enumerate(datasets["srl_tags"]):
        word_ids = tokenized_inputs.word_ids(batch_index=i)
        previous_word_idx = None
        label_ids = []
        for word_idx in word_ids:
            if word_idx is None:
                label_ids.append(-100)
            elif word_idx != previous_word_idx:
                label_ids.append(label[word_idx])
            else:
                label_ids.append(label[word_idx] if label_all_tokens else -100)
            previous_word_idx = word_idx

        labels.append(label_ids)

    tokenized_inputs["labels"] = labels
    return tokenized_inputs


directory = pathlib.Path(__file__).parent.parent


def main():
    """
    Main function which calls the whole script.
    """

    input_args = parse_commandline_arguments()

    tagged_file = input_args.data
    df = pd.read_csv(tagged_file, sep=";", converters={'tokens': eval, 'srl_tags': eval})

    # select relevant columns
    df = df[['sentence_id', 'tokens', 'srl_tags']]
    # print(df_to_train)
    # df_to_train.to_csv(directory / "resource" / "finetune_BERTje" / "train_data.csv", sep=';')

    # check and clean dataframe
    df = drop_rows(df)
    desired_srl_tags = determine_set_of_desired_srl_tags(df)
    df = replace_unknown_tags(df, desired_srl_tags)
    df = encode_srl_tags(df, desired_srl_tags)

    # create dataset objects from dataframe
    datasets = create_dataset_object(df)

    # tokenize datasets
    tokenized_datasets = datasets.map(tokenize_and_align_labels, batched=True)

    # save to disk
    tokenized_datasets.save_to_disk(input_args.output)


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data',
                        help="path to annotated data")
    parser.add_argument('-o', '--output',
                        help="path to preprocessed dataset")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
