import argparse
import pathlib
from typing import Tuple
from transformers import AutoTokenizer, AutoModelForTokenClassification, TrainingArguments, \
    DataCollatorForTokenClassification, Trainer
from datasets import load_from_disk, load_metric
import itertools
import numpy as np

directory = pathlib.Path(__file__).parent.parent


def load_encoded_dataset(path: str):
    """
    Load already preprocessed annotations that were saved in an encoded dataset

    Args:
        path: the path to the encoded dataset

    Returns:
        reloaded_encoded_dataset: the preprocessed annotations that were saved in an encoded dataset
    """
    reloaded_encoded_dataset = load_from_disk(path)
    return reloaded_encoded_dataset


def get_token_classification_model(encoded_dataset, pretrained_model):
    """
    Get a token classification model using a pretrained model and your preprocessed dataset

    Args:
        encoded_dataset: the preprocessed annotations that were saved in an encoded dataset
        pretrained_model: the pretrained BERT model to be loaded

    Returns:
        a pretrained token classification model that can be finetuned
    """
    srl_tags_set = set(itertools.chain.from_iterable(encoded_dataset['train']['srl_tags_name']))
    model = AutoModelForTokenClassification.from_pretrained(pretrained_model,
                                                            num_labels=len(srl_tags_set))
    return model


def set_training_arguments(batch_size=1, checkpoint_folder="."):
    """
    To instantiate a Trainer, we need to define (among other things) TrainingArguments, which is a class that contains
    all the attributes to customize the training.

    Args:
        batch_size:
        checkpoint_folder: a folder that will be used to save checkpoints of the model

    Returns:
        args: the training arguments class

    """
    args = TrainingArguments(
        output_dir=checkpoint_folder,
        evaluation_strategy="epoch",
        learning_rate=2e-5,
        # per_device_train_batch_size=batch_size,
        # per_device_eval_batch_size=batch_size,
        num_train_epochs=4,
        weight_decay=0.01
    )
    return args


def compute_metrics(predictions_labels: Tuple) -> dict:
    """
    To instantiate a Trainer, we need to define (among other things) how to compute the metrics from the predictions.
    We load the seqeval metric (commonly used to evaluate results on the CONLL dataset) via the Datasets library.
    We do some post-processing on our predictions:
    - select the predicted index (with the maximum logit) for each token
    - convert it to its string label
    - ignore if label is -100
    This function does all this post-processing on the result of Trainer.evaluate (which is a namedtuple
    containing predictions and labels) before applying the metric.

    Args:
        predictions_labels: tuple of predictions and labels

    Returns:
        a dictionary containing the results (precision, recall, f1 and accuracy)
    """
    predictions, labels = predictions_labels
    predictions = np.argmax(predictions, axis=2)
    label_list = ["O", "Action", "Actor", "Object", "Recipient"]

    true_predictions = [
        [label_list[predictions_labels] for (predictions_labels, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]
    true_labels = [
        [label_list[l] for (predictions_labels, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]

    metric = load_metric("seqeval")
    results = metric.compute(predictions=true_predictions, references=true_labels)
    return {
        "precision": results["overall_precision"],
        "recall": results["overall_recall"],
        "f1": results["overall_f1"],
        "accuracy": results["overall_accuracy"],
    }


def finetune_model(encoded_dataset, model, tokenizer, args, data_collator, outputdir):
    """
    We instantiate a Trainer object and can then finetune our model by just calling the train method.

    Args:
        encoded_dataset: the preprocessed annotations that were saved in an encoded dataset
        model: a pretrained model that can be finetuned
        tokenizer: a pretrained tokenizer
        args: the training arguments class that contains all the attributes to customize the training
        data_collator: a pretrained data_collator

    Returns:
        the finetuned model
    """
    trainer = Trainer(
        model=model,
        args=args,
        train_dataset=encoded_dataset["train"],
        eval_dataset=encoded_dataset["validation"],
        data_collator=data_collator,
        tokenizer=tokenizer,
        compute_metrics=compute_metrics
    )
    trainer.train()
    trainer.save_model(outputdir)
    return AutoModelForTokenClassification.from_pretrained(outputdir)


def main():
    """
    Main function which calls the whole script.
    We load: our encoded dataset, a pretrained model, tokenizer and data collator, and we set the training arguments.
    The annotations collator will batch our processed examples together while applying padding to make them all the same
    size (each pad will be padded to the length of its longest example).
    """
    input_args = parse_commandline_arguments()

    encoded_data = load_encoded_dataset(input_args.dataset)
    pretrained_model = get_token_classification_model(encoded_data, input_args.model)
    tokenizer = AutoTokenizer.from_pretrained(input_args.model)
    eval_args = set_training_arguments()
    data_collator = DataCollatorForTokenClassification(AutoTokenizer.from_pretrained("GroNLP/bert-base-dutch-cased"))

    finetune_model(encoded_data, pretrained_model, tokenizer, eval_args, data_collator, input_args.outputdir)


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model',
                        help="pretrained model")
    parser.add_argument('-d', '--dataset',
                        help="location of your preprocessed annotated dataset")
    parser.add_argument('-o', '--outputdir',
                        help="save location")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
