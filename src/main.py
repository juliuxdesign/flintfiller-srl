import argparse
from label_text import label_sentences, label_sequence, label_encoded_set
from labels_to_frames import create_frames


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data',
                        help="data path")
    parser.add_argument('-s', '--seq',
                        help="sequence")
    parser.add_argument('-t', '--tokenizer',
                        help="pretrained model as tokenizer")
    parser.add_argument('-m', '--model',
                        help="your finetuned model")
    parser.add_argument('-o', '--output',
                        help="save location")
    parser.add_argument('-f', '--flintoutput',
                        help="desired save location of your flint frames")
    args = parser.parse_args()
    return args


def main():
    args = parse_commandline_arguments()
    if args.data:
        if args.data.endswith('.csv'):
            label_sentences(args.data, args.tokenizer, args.model, args.output, False)
        else:
            label_encoded_set(args.data, args.tokenizer, args.model, args.output)
        create_frames(args.output, args.flintoutput)
    elif args.seq:
        label_sequence(args.seq, args.tokenizer, args.model)
    else:
        print('please enter the path of your data or a sequence.')


if __name__ == '__main__':
    main()
